
const express = require('./core-builders/express')
const config = require('./environments')
const logger = require('./utils/logger')

var app, http

async function createApp() {
    app = await express()
    http = require('http').createServer(app)
    http.listen(config.PORT, config.HOST)
    logger.logMessage(`Application running at http://${config.HOST}:${config.PORT}`)
}

createApp();
