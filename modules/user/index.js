
const signinUser = require('./lib/signinUser')

module.exports = {
    '/auth': {
        '/signin': {
            post: [signinUser]
        }
    }
}
