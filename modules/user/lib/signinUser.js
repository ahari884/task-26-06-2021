const Joi = require('joi');
const jwt = require('jsonwebtoken');
const { logMessage } = require('../../../utils/logger');
const config = require('../../../environments');

const SCHEMA = Joi.object({
    username: Joi.string().min(3).max(35).required(),
    password: Joi.string().min(3).max(35).required(),
    isAdmin: Joi.boolean().optional().default(false)
});

const requestHanlder = async function (req, res) {
    let body = req.body;
    let doesHaveError;
    await SCHEMA.validateAsync(body).catch(e=>{
        logMessage('Error validating');

        doesHaveError = e;
    })

    if (doesHaveError) {
        return res.status(400).send({
            status: 'failed',
            message: doesHaveError.details[0].message,
            alertMessage: 'Validation failed'
        })
    }

    let token = jwt.sign({
        username: body.username,
        isAdmin: body.isAdmin ? 'yes': 'no'
    }, config.SECRET, { expiresIn: '24h' });
      

    return res.status(200).send({
        message: 'Successfully Logged In',
        accessToken: token
    });
}

module.exports = requestHanlder
