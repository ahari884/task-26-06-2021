const { default: axios } = require('axios');
const { logMessage } = require('../../../utils/logger');
const Joi = require('joi');
const config = require('../../../environments');

const SCHEMA = Joi.object({
  postId: Joi.string().required()
});

const LIMIT = 10;

const requestHandler = async function(req, res) {
    let doesHaveError;
    let postId = req.params.postId;
    if ( postId) {
        await SCHEMA.validateAsync({ postId }).catch(e=>{
            logMessage('Error validating');
            doesHaveError = e;
        })
        
        if (doesHaveError) {
            return res.status(400).send({
                status: 'failed',
                message: doesHaveError.details[0].message,
                alertMessage: 'Validation failed'
            })
        }
    }
    
    let pageNumber = req.query.pageNumber || 1;
    let URL = config.JSON_SERVER + '/posts/' + (postId ? `?id=${postId}` : `?_page=${pageNumber}&&_limit=${LIMIT}`);
    let post
    await axios.get(URL).then(result=>{
        if(result && result.data && result.data.length) {
            if(postId) post = result.data[0];
            else post = result.data
        }
    }).catch((e) => {
        logMessage('Error creating post');
        console.log(e);
    })

    console.log('Post ', post);

    if (post) {
        return res.status(200).send({
            status: 'success',
            message: 'Successfully fetched the post',
            post
        })
    }
    res.status(500).send({
        status: 'failed',
        message: 'Something went wrong deleting the post'
    })
}

module.exports = requestHandler;
