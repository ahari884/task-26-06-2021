const { default: axios } = require('axios');
const config = require('../../../environments');
const { logMessage } = require('../../../utils/logger');
const Joi = require('joi');
const uuid = require('uuid');

const SCHEMA = Joi.object({
    post: Joi.string().min(1).max(255).required(),
    postId: Joi.string().required()
})

const requestHandler = async function (req, res) {
    let post = req.body.post
    let postId = req.params.postId;
    let doesHaveError;
    await SCHEMA.validateAsync({
        post,
        postId
    }).catch(e => {
        logMessage('Error validating');
        doesHaveError = e;
    })

    if (doesHaveError) {
        return res.status(400).send({
            status: 'failed',
            message: doesHaveError.details[0].message,
            alertMessage: 'Validation failed'
        })
    }

    let postBody = {
        post
    }

    let updatedPost = await axios.put(config.JSON_SERVER + '/posts/' + postId, postBody).catch((e) => {
        logMessage('Error creating post');
        console.log(e);
    })

    if (updatedPost) {
        return res.status(201).send({
            status: 'success',
            message: 'Successfully updated the post',
            post: postBody
        })
    }

    return res.status(500).send({
        status: 'failed',
        message: 'Something went wrong creating the post'
    })

}

module.exports = requestHandler;
