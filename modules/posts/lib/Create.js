const { default: axios } = require('axios');
const config = require('../../../environments');
const { logMessage } = require('../../../utils/logger');
const Joi = require('joi');
const uuid = require('uuid');

const SCHEMA = Joi.object({
    post: Joi.string().min(1).max(255).required()
})

const requestHandler = async function(req, res) {
    let body = req.body;
    let doesHaveError;
    await SCHEMA.validateAsync(body).catch(e=>{
        logMessage('Error validating');
        doesHaveError = e;
    })
    
    if (doesHaveError) {
        return res.status(400).send({
            status: 'failed',
            message: doesHaveError.details[0].message,
            alertMessage: 'Validation failed'
        })
    }

    let postBody = {
        id: uuid.v4(),
        post: body.post
    }

    let post = await axios.post(config.JSON_SERVER + '/posts', postBody).catch((e) => {
        logMessage('Error creating post');
        console.log(e);
    })

    if(post) {
        return res.status(200).send({
            status: 'success',
            message: 'Successfully created the post',
            post: postBody
        })
    }

    return res.status(500).send({
        status: 'failed',
        message: 'Something went wrong creating the post'
    })
}

module.exports = requestHandler
