const { default: axios } = require('axios');
const { logMessage } = require('../../../utils/logger');
const Joi = require('joi');
const config = require('../../../environments');

const SCHEMA = Joi.object({
  postId: Joi.string().required()
})

const requestHandler = async function(req, res) {
    let doesHaveError;
    let postId = req.params.postId;
    await SCHEMA.validateAsync({ postId }).catch(e=>{
        logMessage('Error validating');
        doesHaveError = e;
    })
    
    if (doesHaveError) {
        return res.status(400).send({
            status: 'failed',
            message: doesHaveError.details[0].message,
            alertMessage: 'Validation failed'
        })
    }

    let post = await axios.delete(config.JSON_SERVER + '/posts/' + postId).catch((e) => {
        logMessage('Error creating post');
        console.log(e);
    })

    if (post) {
        return res.status(200).send({
            status: 'success',
            message: 'Successfully delete the post'
        })
    }
    res.status(500).send({
        status: 'failed',
        message: 'Something went wrong deleting the post'
    })
}

module.exports = requestHandler;
