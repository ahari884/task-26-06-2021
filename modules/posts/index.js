const Create = require('./lib/Create');
const Delete = require('./lib/Delete');
const Update = require('./lib/Update');
const Get = require('./lib/Get');
const express = require('express');

module.exports = {
    '/posts': {
        get: [Get],
        post: [Create],
        '/:postId': {
            get: [Get],
            delete: [Delete],
            put: [Update]
        }
    }
}
