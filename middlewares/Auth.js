
const jwt = require('jsonwebtoken');
const config = require('../environments');

const validateUserToken = async function (req, res, next) {
    let token = req.headers.Authorization || req.headers.authorization;

    if (!token)
        return res.status(400).send({
            status: 'failed',
            messsage: 'Authorization token missing in the headers'
        });

    let isValid;
    try {
        isValid = await jwt.verify(token, config.SECRET);
    } catch (e) {
        // just-catcing-the-error no-use-error-block
    }

    req.user = isValid;

    if (!isValid)
        res.status(401).send({
            messsage: 'Token expired or invalid'
        })

    next();
};

const isAuthorised = function (isAdmin) {
    return function (req, res, next) {
        if (req.user.isAdmin === isAdmin) {
            return next();
        } else {
            return res.status(403).send({
                messsage: 'You are not authorised to access. Please contact your admin'
            })
        }
    }
}

module.exports = {
    validateUserToken,
    isAuthorised
}