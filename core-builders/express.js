
const express = require('express')
const morgan = require('morgan')
const compress = require('compression')
const methodOverride = require('method-override')
const bodyParser = require('body-parser')
const path = require('path')
const glob = require('glob')

const config = require('../environments')
const logger = require('../utils/logger')
const { validateUserToken, isAuthorised } = require('../middlewares/Auth')

const ENV = process.env.NODE_ENV || 'development'
const METHODS = ['get', 'post', 'put', 'delete']

const expressApp =  async function() {
    
    var app = express()

    // managing CORS
    app.use( (req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        res.header("Access-Control-Allow-Methods","PUT, GET, POST, OPTIONS, DELETE");
        next()
    })

    // initilizing static folder for uploads and assets
    app.use(express.static(config.UPLOADS_DIR))

    // morgan logger
    if(ENV === 'development') {
        app.use(morgan('dev'));
    } else {
        app.use(compress());
    }

    app.use(bodyParser.urlencoded({
        extended:true
    }))

    app.use(bodyParser.json())
    app.use(methodOverride())

    // logger for request
    app.use((req, res, next) => {
        logger.logRequest(req)
        next()
    })

    app.use((req, res, next) => {
        logger.logResponse(res)
        next()
    })

    app.set('base', '/api')

    glob(path.join(__dirname, '../modules/*/index.js'), (err, routes) => {
        if(err) {
            logger.logError(`Error loading routes : ${JSON.stringify(err)}`)
            process.exit(1)
        }

        if(!(routes && routes.length)) {
            logger.logError('Routes not found! Please add routes to the project.')
            process.exit(1)
        }

        console.log('Routes', routes);

        for(let route of routes) {
            apiLoader(app, require(route), '')
        }
        
    })

    app.all('/posts/*', validateUserToken);
    app.post('/posts/*', validateUserToken, isAuthorised('yes'));
    app.delete('/posts/*', validateUserToken, isAuthorised('yes'));
    app.put('/posts/*', validateUserToken, isAuthorised('yes'));
    

    return app

}

const apiLoader = function (app, route, compositeEndpoint) {
    for(let prefix of Object.keys(route)) {
        if( typeof(prefix) !== 'object' && METHODS.includes(prefix) ) {

            let method = prefix
            let callbacks = route[method]

            if(callbacks && callbacks.length) {
                logger.logAPILoading(compositeEndpoint, method)
                app[method](compositeEndpoint, ...callbacks)
            } else {
                logger.logError(`No callback functions provided for API ${compositeEndpoint}`)
            }

            continue
        }
        apiLoader(app, route[prefix], compositeEndpoint + prefix)
    }

}


module.exports = expressApp
