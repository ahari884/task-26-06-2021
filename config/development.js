
const path = require('path')

const configuration = {
    PROTOCOL: 'http',
    ENV: 'development',
    PORT: 3000,
    HOST: 'localhost',
    UPLOADS_DIR: path.join(__dirname, '../uploads'),
    SECRET: 'SecretKeyGoesHere',
    JSON_SERVER: 'http://localhost:5000'
}

module.exports = configuration
