
const chalk = require('chalk')

const error = chalk.bold.red
const warning = chalk.bold.keyword('orange')
const API = chalk.bold.white.underline
const success = chalk.bold.green
const messageLog = chalk.yellowBright
const log = console.log

const logRequest = function(req) {
    log(success(req))
}

const logResponse = function(res) {

}

const logMessage = function(message) {
    log(messageLog(message))
}

const logError = function(message) {
    log(error(message))
}

const logAPILoading = function (api, method) {
    log(`\n${API(method.toUpperCase())+' : '+api}${warning(' -> ')}${success('loaded successfully')}\n`)
} 

module.exports = {
    logRequest,
    logResponse,
    logMessage,
    logError,
    logAPILoading
}
