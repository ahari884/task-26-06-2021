## Installation
To install the packages, simply cd to the project directory and npm install

## Running the application

Simply enter npm start it will run on port number 3000


## APIS
1) /auth/signin : mehtod post; body - { "username": any, "password": any, isAdmin: boolean }
2) /posts : methodds - [post, get]; body - {"post": string}
3) /post/:postId: methods - [put, get, delete] - {"post": string}

### Headers
1) Content-Type - application/json
2) Authorization - Token from signin response

## Database Server API - json-server
To manipulate the data changes used json-server package where can manipulate the mockAPIs

1) To run this install **json-server** npm package globally and then create a json file
2) Then cd to the folder of that json file
3) And run this command
**json-server -p 5000 --watch file-name.json**